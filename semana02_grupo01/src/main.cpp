
#include <iostream>
using namespace std;

//Problema 1
void prob01(){
	int nota1, nota2, nota3;
	cout << "Ingrese nota PC1: ";
	cin >> nota1;

	cout << "Ingrese nota PC2: ";
	cin >> nota2;

	cout << "Ingrese nota PC3: ";
	cin >> nota3;

	float promedio = 1.0 * (nota1 + nota2 + nota3) / 3;
	cout << "Promedio: " << promedio;
}

//Problema 2
void prob02(){
	int nota1, nota2, nota3, nota4;
	int menor = 0;

	cout << "Ingrese nota PC1: ";
	cin >> nota1;
	menor = nota1;

	cout << "Ingrese nota PC2: ";
	cin >> nota2;
	if(menor > nota2){
		menor = nota2;
	}

	cout << "Ingrese nota PC3: ";
	cin >> nota3;
	if(menor > nota3){
		menor = nota3;
	}

	cout << "Ingrese nota PC4: ";
	cin >> nota4;
	if(menor > nota4){
		menor = nota4;
	}

	float promedio = 1.0 * (nota1 + nota2 + nota3 + nota4 - menor) / 3;
	cout << "Promedio : " << promedio;
}

//Problema 3
float obtenerValorIgv(float monto){
	return 0.18 * monto;
}

void prob03(){
	float precio;
	cout << "Ingrese precio: ";
	cin >> precio;
	cout << "Valor del IGV: " << obtenerValorIgv(precio);
}

float obtenerTotalFactura(float monto){
	return monto + obtenerValorIgv(monto);
}

//Problema 4
void prob04(){
	float monto;
	cout << "Ingrese subtotal: ";
	cin >> monto;
	cout << "Total factura: " << obtenerTotalFactura(monto);
}

int main() {
	//prob01();
	//prob02();
	//prob03();
	prob04();
	return 0;
}
